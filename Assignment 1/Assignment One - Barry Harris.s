# Programming Assignment 3

.data
header:     .asciiz "\nAssignment #3 Program!\n-----------------------\n"
prompt:     .asciiz "\nPlease enter an integer "
prompt3:    .asciiz "Hello\n"
prompt2:    .asciiz "\nPlease enter the number of integers to convert "
answer:     .asciiz "\nThe hexadecimal value of decimal integer "
answer2:    .asciiz " is: "
endline:    .asciiz "\n"
enter:      .asciiz "\n"
array:      .space 20

.text
main:
# Print Header
li $v0, 4                           # System call for outputting strings
la $a0, header                      # Loading word into registar
syscall

# Load array for insertion of integers
la $t3, array                       # Array pointer for traversing pointer

#Load the number of integers to convert
li $v0, 4
la $a0, prompt2
syscall
li $v0, 5
syscall

beq $v0, $zero, main

# Loop to read & strore input integers
add $t0, $zero, $zero               # initialize counter to 0
move $t1, $v0                       # initialize max value to user input
insert_loop: beq $t0, $t1, exit_loop
    li $v0, 4                       # System call for outputting strings
    la $a0, prompt                  # Loading word into registar
    syscall
    li $v0, 5
    syscall
    move $t2, $v0
    blt $t2, $zero, insert_loop
    bgt $t2, 32768, insert_loop
    sw $t2, ($t3)                   # Inserts integer into if it passes checks
    li $v0, 1
    addi $t3, $t3, 4                # Increments the array pointer by 4
    addi $t0, $t0, 1                # increment counter by 4
    j insert_loop
exit_loop:
    la $t3, array
    li $t0, 0
    printloop: beq $t0, $t1, exit
        addi $sp, $sp, -8           # Reserve space in the stack

        # Call the subprogram
        lw $t5, ($t3)
        sw $t5, 0($sp)

        jal tohex

        lw $s2, 4($sp)              # Move result from return register to temp register

        # Print the formatted result
        li $v0, 4
        la $a0, answer
        syscall

        li $v0, 1
        lw $a0, ($t3)
        syscall

        li $v0, 4
        la $a0, answer2
        syscall

        andi $s1, $s2, 255              # First Hex value
        li $v0,11
        move $a0, $s1
        syscall

        srl $s2, $s2, 8

        andi $s1, $s2, 255              # Second Hex value
        li $v0, 11
        move $a0, $s1
        syscall

        srl $s2, $s2, 8

        andi $s1, $s2, 255              # Third Hex value
        li $v0, 11
        move $a0, $s1
        syscall

        srl $s2, $s2, 8

        andi $s1, $s2, 255              # Fourth Hex Value
        li $v0, 11
        move $a0, $s1
        syscall

        addi $t3, $t3, 4                # Increment the array pointer by 4
        addi $t0, $t0, 1                # Increment counter by 4

        addi $sp, $sp, 8

        j printloop
exit:
    li $v0, 4
    la $a0, enter
    syscall

    li $v0, 10
    syscall
tohex:
    add $t4, $zero, $zero               # Start counter
    li $t7, 4                           # Star counter high bound
    lw $t5, 0($sp)
    tohex_loop: beq $t4, $t7, exit_tohex_loop
        li $t2, 0
        andi $t2, $t5, 15               # Perform Logical AND on 15 and parameter
        sll $t8, $t8, 8
        blt $t2, 10, less
        bge $t2, 10, greater
    less:
        addi $t2, $t2, 48               # Adding result of logical AND to ASCII '0'
        sra $t5, $t5, 4                 # Shifting param 4 bits to right
        # Storing lower 8 bits from result
        add $t8, $t8, $t2               # Placing result in register
        addi $t4, $t4, 1                # Increment counter
        j tohex_loop
    greater:
        addi $t2, $t2, 55               # Adding result of logical AND to ASCII 'A'
        sra $t5, $t5, 4                 # Shifting param 4 bits to right
        # Storing lower 8 bits
        add $t8, $t8, $t2               # Placing result in register
        addi $t4, $t4, 1                # Increment counter
        j tohex_loop
    exit_tohex_loop:
        sw $t8, 4($sp)
        jr $ra
